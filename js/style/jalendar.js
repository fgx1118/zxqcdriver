  var ww=null;
$(function ()
{
    (function ($) 
    {
        $.fn.jalendar = function (options) 
        {
            
            var settings = $.extend({
                customDay: new Date(),
                color: '#ef3a27',
                busicolor:'#4ac7d9',
                lang: 'CN'
            }, options);
            
            var ifbegin=0;
            // Languages
            var dayNames = {};
            var monthNames = {};
            var lAddEvent = {};
            var lAllDay = {};
            var lAllDay2 = {};
            var lAllDay3 = {};
            
            var lTotalEvents = {};
            var lEvent = {};
            dayNames['EN'] = new Array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
            dayNames['CN'] = new Array('一', '二', '三', '四', '五', '六', '日');
            
            dayNames['TR'] = new Array('Pzt', 'Sal', 'Çar', 'Per', 'Cum', 'Cmt', 'Pzr');
            dayNames['ES'] = new Array('Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Såb', 'Dom');
            
            monthNames['EN'] = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
            monthNames['CN'] = new Array('1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月');
            
            monthNames['TR'] = new Array('Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık');
            monthNames['ES'] = new Array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'); 
          
            lAddEvent['EN'] = 'Add New Event';
            lAddEvent['CN'] = '添加新行程';
            lAddEvent['TR'] = 'Yeni Etkinlik Ekle';
            lAddEvent['ES'] = 'Agregar Un Nuevo Evento';
            
            lAllDay['EN'] = 'ALL DAY';
            lAllDay['CN'] = '繁忙日期(不接单)';
            lAllDay['TR'] = 'Tüm Gün';
            lAllDay['ES'] = 'Todo El Día';
            
            lAllDay2['EN'] = '服务日期';
            lAllDay2['CN'] = '服务日期';
            
            lAllDay2['TR'] = 'Tüm Gün';
            lAllDay2['ES'] = 'Todo El Día';
            
        //    lAllDay3['EN'] = '空闲日期';
         //   lAllDay3['TR'] = 'Tüm Gün';
          //  lAllDay3['ES'] = 'Todo El Día';
            
            
            lTotalEvents['EN'] = 'Total Events in This Month: ';
            lTotalEvents['CN'] = '本月总繁忙日程: ';
            
            lTotalEvents['TR'] = 'Bu Ayki Etkinlik Sayısı: ';
            lTotalEvents['ES'] = 'Total De Eventos En Este Mes: ';
            
            lEvent['EN'] = 'Event(s)';
            lEvent['CN'] = '行程数';
            lEvent['TR'] = 'Etkinlik';
            lEvent['ES'] = 'Evento(s)';
            
            
            var $this = $(this);
            var div = function (e, classN) {
                return $(document.createElement(e)).addClass(classN);
            };
            
            var clockHour = [];
            var clockMin = [];
            for (var i=0;i<24;i++ ){
                clockHour.push(div('div', 'option').text(i))
            }
            for (var i=0;i<59;i+=5 ){
                clockMin.push(div('div', 'option').text(i))
            }
            
            // HTML Tree
            $this.append(
                div('div', 'wood-bottom'), 
                div('div', 'jalendar-wood').append(
                    div('div', 'close-button'),
                    div('div', 'jalendar-pages').append(
                        div('div', 'pages-bottom'),
                        div('div', 'header').css('background-color', 'white').append(
                            div('a', 'prv-m'),
                            div('h1'),
                            div('a', 'nxt-m'),
                            div('div', 'day-names')
                        ),
                        div('div', 'total-bar').html( lTotalEvents[settings.lang] + '<b style="color: '+settings.color+'"></b>'),
                        div('div', 'days')
                    ), 
                    div('div', 'text').html("<img src='../images/new/calendar.png' style='width:100%;border-top:1px solid #f0f0f0'>"),
                    div('div', 'submit').html("<div style='margin:10px auto;font-size:16px;background:#4ac7d9;padding-top:10px;padding-bottom:10px;border-radius:2px;'>保 存</div>")
                    
                    
                    /*,
                    div('div', 'add-event').append(
                        div('div', 'add-new').append(
                            '<div class=dddiv><input type="text" placeholder="' + lAddEvent[settings.lang] + '" value="' + lAddEvent[settings.lang] + '" style=\"display:none\"/></div>',
                            div('div', 'submit').text("提交"),
                            
                            
                          
                            
                            div('div', 'clear'),
                            div('div', 'add-time').append(
                                div('div', 'disabled'),
                                div('div', 'select').addClass('hour').css('background-color', settings.color).append(
                                    div('span').text('00'),
                                    div('div', 'dropdown').append(clockHour)
                                ),
                                div('div', 'left').append(':'),
                                div('div', 'select').addClass('min').css('background-color', settings.color).append(
                                    div('span').text('00'),
                                    div('div', 'dropdown').append(clockMin)
                                )
                            ),
                            
                            div('div', 'all-day').append(
                                '<div class=ddiv><span><input type="checkbox" id="state" name="state" placeholder="' + lAllDay[settings.lang] + '" value="' + lAllDay[settings.lang] + '"/></span><span>'+lAllDay[settings.lang]+"</span></div>"
                            )
                 
                           ,
                     
                            div('div', 'clear')
                           
                            
                        ),
                        div('div', 'events').append(
                            div('h3','').append(
                                div('span', '').html('<b></b> ' + lEvent[settings.lang])
                            ),
                            div('div', 'gradient-wood'),
                            div('div', 'events-list')
                        )
                    ) */
                )
            );
            
            // Adding day boxes
            for (var i = 0; i < 42; i++) {
                $this.find('.days').append(div('div', 'day'));
            }
            
            // Adding day names fields
            for (var i = 0; i < 7; i++) {
                $this.find('.day-names').append(div('h2').text(dayNames[settings.lang][i]));
            }

            var d = new Date(settings.customDay);
            var year = d.getFullYear();
            var date = d.getDate();
            var month = d.getMonth();
            
            var isLeapYear = function(year1) {
                var f = new Date();
                f.setYear(year1);
                f.setMonth(1);
                f.setDate(29);
                return f.getDate() == 29;
            };
        
            var feb;
            var febCalc = function(feb) { 
                if (isLeapYear(year) === true) { feb = 29; } else { feb = 28; } 
                return feb;
            };
            var monthDays = new Array(31, febCalc(feb), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

            function calcMonth() 
            {

                monthDays[1] = febCalc(feb);
                
                var weekStart = new Date();
                weekStart.setFullYear(year, month, 0);
                var startDay = weekStart.getDay();  
                
                $this.find('.header h1').html(monthNames[settings.lang][month] + ' ' + year);
        
                $this.find('.day').html('&nbsp;');
                $this.find('.day').removeClass('this-month');
                for (var i = 1; i <= monthDays[month]; i++) {
                    startDay++;
                    
                    $this.find('.day').eq(startDay-1).addClass('this-month').attr('data-date', i+'/'+(month+1)+'/'+year).html(i);
                    $this.find('.day').eq(startDay-1).addClass('this-month').attr('data-date2', i+'-'+(month+1)+'-'+year).html(i);
                    
                }
                if ( month == d.getMonth() ) {
                    $this.find('.day.this-month').removeClass('today').eq(date-1).addClass('today').css('color', settings.color);
                } else {
                    $this.find('.day.this-month').removeClass('today').attr('style', '');
                }
                
            
                
                // added event
                $this.find('.added-event').each(function(i)
                {
                
                 //   $(this).attr('data-id', i);
                    $this.find('.this-month[data-date="' + $(this).attr('data-date') + '"]').append(
                        div('div','event-single').attr('data-id', i).append
                        (
                           div('p','').text($(this).attr('data-title')),
                            div('div','details').append(
                                div('div', 'clock').text($(this).attr('data-time')),
                                div('div', 'erase')
                            )
                        )
                    );
                    $this.find('.day').has('.event-single').addClass('have-event').prepend(div('i',''));
                    $this.find('.day').has('.event-single'); //.attr('style','background:red'); // toma dd
                     
                    //.prepend(div('i',''));
                });
                
                calcTotalDayAgain();  
                 
                 
                 
            }
            
            calcMonth();
            
         
            
            var arrows = new Array ($this.find('.prv-m'), $this.find('.nxt-m'));
            var dropdown = new Array ($this.find('.add-time .select span'), $this.find('.add-time .select .dropdown .option'), $this.find('.add-time .select'));
            var allDay = new Array ('.all-day fieldset[data-type="disabled"]', '.all-day fieldset[data-type="enabled"]');
            var $close = $this.find('.jalendar-wood > .close-button');
            var $erase = $this.find('.event-single .erase');
           
           
            $this.find('.jalendar-pages').css({'width' : $this.find('.jalendar-pages').width() });
            $this.find('.events').css('height', ($this.height()-197) );
            $this.find('.select .dropdown .option').hover(function() {
                $(this).css('background-color', settings.color); 
            }, function(){
                $(this).css('background-color', 'inherit'); 
            });
            var jalendarWoodW = $this.find('.jalendar-wood').width();
            var woodBottomW = $this.find('.wood-bottom').width();

            // calculate for scroll
            function calcScroll() {
                if ( $this.find('.events-list').height() < $this.find('.events').height() ) { $this.find('.gradient-wood').hide(); $this.find('.events-list').css('border', 'none') } else { $this.find('.gradient-wood').show(); }
            }
            
            // Calculate total event again
            function calcTotalDayAgain() 
            {
                
                
                var eventCount = $this.find('.this-month .event-single').length;
                
                $this.find('.total-bar b').text(eventCount);
              
                $this.find('.events h3 span b').text($this.find('.events .event-single').length);
                
                
                 
                
            }
          
          
            //tom add for click 
            function prevAddEventclick() 
            {
               // $this.find('.day').removeClass('selected').removeAttr('style'); //tom canel
              
                 $this.find('.day').not(".have-event").removeClass('selected').removeAttr('style');  // tom add
                $this.find('.today').css('color', settings.color);
                $this.find('.add-event').hide();
             
                jalendarWoodW=woodBottomW; //tom add
                $this.children('.jalendar-wood').animate({'width' : jalendarWoodW}, 200);
                 
                   
                $this.children('.wood-bottom').animate({'width' : woodBottomW}, 200);
                $close.hide();
            }
            // tom add ed
            
            
            
            function prevAddEvent() 
            {
            	
                $this.find('.day').removeClass('selected').removeAttr('style');
                $this.find('.today').css('color', settings.color);
                $this.find('.add-event').hide();
             
                jalendarWoodW=woodBottomW; //tom add
                $this.children('.jalendar-wood').animate({'width' : jalendarWoodW}, 200);
                 
                   
                $this.children('.wood-bottom').animate({'width' : woodBottomW}, 200);
                $close.hide();
            }
            
            function refreshcalendar()
            {
            
            	//--删除掉多余的一行
            	 var dayslist= $('.days').find(".day");
            	 
            	   var xcount=0;
            	   for (var i = 0; i < 42; i++)
            	   {
                   
                   var s=$.trim(dayslist[41-i].innerText);
                   if(i<=6)
                  { 
                  	
                   if(s=="")
                   {
                   	//alert(s);
                   	 xcount=xcount+1;
                   }
                  } 
                  // $this.find('.days').append(div('div', 'day'));
                 }
            	 //	alert(xcount);
            	   if(xcount==7)
            	   {
            	   	
            	   	for (var i = 0; i < 42; i++)
            	   {
                   
                   var s=$.trim(dayslist[41-i].innerText);
                   if(i<=6)
                  { 
                  //	alert(s);
                   if(s=="")
                   {
                   
                    dayslist[41-i].setAttribute("style","display:none");
                   }
                  } 
                  // $this.find('.days').append(div('div', 'day'));
                 }
            	   	
            	   }
            	
            	 //---删除结束
            	 
            	  
            	 var mstr=$(".header h1").text();
            	// mstr.replaceAll(" ","");
            	 var xindex=mstr.indexOf("月");
            	 var the_month=mstr.substring(0,xindex);
            	 var the_year=mstr.substring(mstr.length-4,mstr.length);
            
         
            
        
         //   return;
            	
            	   
         //    var w=plus.nativeUI.showWaiting("　　 正在获取数据  请等待...　　 \n");   
                
           	 var didvalue=localStorage.getItem('did');
           	// didvalue=2;
   
                $.ajax({ type: "POST",
           url:"http://www.etjourney.com/project/baoche/app/server/refreshcalendar.php", 
           data:"did="+didvalue+"&month="+the_month+"&year="+the_year,
           dataType:"jsonp",
           jsonp:'callback', 
           timeout:30000,
           success: function(msg, textStatus)
           {
           	 //	w.close();
           	   if(msg.success=="1")
           	   {
           	   	
          // 	   //	alert(1);
     //----------开始刷新日历---------------------
                   var m_count=msg.count;
              
                
                if(m_count<=0)
                  {
                  	   
                  	
                  }
                  else
                  {
                  	  var record=msg.record;
                  	  if(record!=null)
                  	  {
                  	  	    var s=$this.find(".this-month");
                  	  	    
    	      	              if(s!=null)
    	      	              {
    	      	              	
    	      	              	 
    	      	              	      for(var a=0;a < s.length;a++)
    	      	              	      {
    	      	              	      
    	      	                           for(var i=0;i<record.length;i++)
    	      	                           {
    	      	                           	   
    	      	                           	     if(s[a].getAttribute("data-date")==record[i].day)
    	      	                           	     {
    	      	                           	    // 	alert(s[a].getAttribute("data-date"));
    	      	                           	      s[a].setAttribute('class','day this-month selected');
    	      	                              	//s[a].css({'background-color':settings.color});
    	      	                              	
    	      	                              	 s[a].setAttribute("style","background:"+settings.color);
    	      	                             // 	   s[a].css({"width":(ww-12)});
                                              //    s[a].css({"height":(ww-12)});

    	      	                           	  
    	      	                           	  /*
    	      	                           	     	 var ss=record[i].day.replace("/","-");
    	      	                           	     	 ss=ss.replace("/","-");

                                                     s[a].setAttribute('data-date2',ss);
    	      	                           	     	 
    	      	                           	     	 var xarray=record[i].day.split("/");
    	      	                           	     	 
    	      	                           	     	 var sss="<i></i><div class=\"event-single\" data-id=\""+ss+"\"><p></p><div class=\"details\"><div class=\"clock\">"+record[i].day+"</div><div class=\"erase\" data-datex=\""+record[i].day+"\"></div></div></div>";
    	      	                           	     	 sss=sss+xarray[0];
    	      	                           	     	 s[a].innerHTML=(sss);
    	      	                           	     	 alert(a);
    	      	                           	     s[a].setAttribute("style","background-color:red");
    	      	                           	     	
    	      	                           	   //  s[a].css({'background-color':settings.color});
    	      	                           	      
    	      	                           	     	
    	      	                           	     	// s[a].innerHTML= "<i></i>"+);
    	      	                           	     
    	      	                           	     	
    	      	                           	     	
    	      	                           	     	
    	      	                           	     // s[a].setAttribute('class','day this-month selected have-event');
    	      	                           	     //	 s[a].setAttribute('style','background:red');
    	      	                           	     	
    	      	                           	     	//  s[a].setAttribute("class",'day this-month selected have-event');//.addClass('have-event');
    	      	                           	      //  s[a].addClass('selected').addClass('have-event').css({'background-color':settings.color});
    	      	                           	      
*/
    	      	                           	     }
    	      	                           }
    	      	              	      }
    	      	              }
    	      	        }
    	      	       
    	      	        
    	      	//    return;    
    	      	        //---------------------刷新表示
    	      	        
    	      	        
    	     /* 	var eventSingle = $(this).find('.event-single')
                $this.find('.events .event-single').remove();
                
                 
                 prevAddEvent();  /// tom  cancel 20150530
           
                $(this).addClass('selected').css({'background-color': settings.color});
                
                    $this.find('.add-event').show().find('.events-list').html(eventSingle.clone())
                    $this.find('.add-new input').select();
                    calcTotalDayAgain();
                    calcScroll();
              
                 var s=document.getElementById("state");
              //   s.checked=false;
    	      	        
    	      	     */
    	      	    //---------------------END
    	      	        
                  
                  }
     //------------日历刷新结束------------------
           	     
           	   }
           	   else
           	   {
           	 // 	plus.nativeUI.toast(msg.errinfo);
           	   	
           	   }
           	  	
           	  	
           	  	 ww=$(window).width(); 
          ww= Math.floor( (ww/7));
          
          $(".day").css({"width":(ww-12)});
          $(".day").css({"height":(ww-12)});

 $(".day").css({"border-radius": Math.floor((ww-12)/2) }); 
           	  	
           	 
           }
           ,complete: function(XMLHttpRequest, textStatus) 
	       { 
	       
	       //   alert("100"+textStatus);
	       }
	       , error:function (XMLHttpRequest, textStatus, errorThrown)
	       {
	       	
	       	 
	       	//	plus.nativeUI.toast(textStatus);
	       //  	w.close();
	       	// alert(XMLHttpRequest.status);
	       //  alert(textStatus);
	       }
	       
	       
	       
	       
	       
      });  
      
              
            	
            
            }
            
            //往右
            arrows[1].on('click', function () 
            {
                if ( month >= 11 ) {
                    month = 0;
                    year++;
                } else {
                    month++;   
                }
                calcMonth();
                
               
                prevAddEvent();
               
                refreshcalendar();
                
                this.ifbegin=1;
                
                
            });
            
            //往左
            arrows[0].on('click', function () 
            {
                dayClick = $this.find('.this-month');
                if ( month === 0 ) {
                    month = 11;
                    year--;
                } else {
                    month--;   
                }
                
                
                calcMonth();
                prevAddEvent();
                
                
                refreshcalendar();
                 this.ifbegin=1;
            });
            
            
            
            $this.on('click', '.this-month', function () 
            {
            		
          //    alert("开始处理单击事件");		
            		
                var eventSingle = $(this).find('.event-single')
                
                var xy=$(this)[0];
                var xname=xy.className;
             
                if(xname.indexOf("selected")>0)
                {
                	
 	$(this).removeClass('selected').css({'background-color':'white'});
                              	
                }
                else
                {
                	
                 $(this).addClass('selected').css({'background-color': settings.color});
                }
           /*   if($this.find('.events .event-single'))
               {
               	
                $this.find('.events .event-single').remove();
               	 
              
               }
               else
               {
               	
                   prevAddEvent();
                  
               }
               */
                
              //  alert($(this).getAttribute("background-color"));
                
                 
                
               	  
          //     /// tom  cancel 20150530
          
          
          //  prevAddEventclick();
               
            //   $(this).filter(".have-event").addClass('selected').css({'background-color':settings.color});
             
                
               // $this.children('.jalendar-wood, .wood-bottom').animate({width : '+=300px' }, 200,
               // function()
               // {
               	
                    $this.find('.add-event').show().find('.events-list').html(eventSingle.clone())
                    $this.find('.add-new input').select();
                    calcTotalDayAgain();
                    calcScroll();
                 //   $close.show();  //tom cancel 20150809
               // });
              //  alert(1);
                 var s=document.getElementById("state");
          //       s.checked=false;
                
            });
            
            dropdown[0].click(function(){
                dropdown[2].children('.dropdown').hide(0);
                $(this).next('.dropdown').show(0);
            });
            dropdown[1].click(function(){
                $(this).parent().parent().children('span').text($(this).text());
                dropdown[2].children('.dropdown').hide(0);
            });
            $('html').click(function(){
                dropdown[2].children('.dropdown').hide(0); 
            });
            $('.add-time .select span').click(function(event){
                event.stopPropagation(); 
            });
            
            $this.on('click', allDay[0], function()
            {
            
            	
                $(this).removeAttr('data-type').attr('data-type', 'enabled').children('.check').children().css('background-color', settings.color);
                dropdown[2].children('.dropdown').hide(0);
                $(this).parents('.all-day').prev('.add-time').css('opacity', '0.4').children('.disabled').css('z-index', '10');
            });
            
            
            
            $this.on('click', allDay[1], function(){
            
                $(this).removeAttr('data-type').attr('data-type', 'disabled').children('.check').children().css('background-color', 'transparent');
                $(this).parents('.all-day').prev('.add-time').css('opacity', '1').children('.disabled').css('z-index', '-1');
            });
            
          
          
          
          // add new event with panel
          
            var dataId = parseInt($this.find('.total-bar b').text());
            $this.find('.submit').on('click', function()
            {
           
              /*  var hour = $(this).parents('.add-new').find('.hour > span').text();
                var min = $(this).parents('.add-new').find('.min > span').text();
                var isAllDay = $(this).parents('.add-new').find('.all-day fieldset').attr('data-type');
                var isAllDayText = $(this).parents('.add-new').find('.all-day fieldset label').text();
                var thisDay = $this.find('.day.this-month.selected').attr('data-date');
               // alert(thisDay);
                dataId=thisDay.replace("/","-");
                dataId=dataId.replace("/","-");
             //   alert(dataId);
             
                var ifsel=0;   
                if(document.getElementById("state").checked==true)
                {
                	ifsel=1;
                
                }
             
             
             
             
            //  alert(ifsel);
               var title = $(this).prev('input').val();
                
            
                var time;
               
               
                 if ( ifsel <=0)
                {
                    time = hour + ':' + min;
                    
                    
                    
                } 
                else 
                {
                    time = title  ;//isAllDayText;
                }
                
               var ifself=document.getElementById("event-"+dataId);
               
              */  
                	//   alert("ifsel=0");
                		//tom cancel 20150809
                		/*
                if(ifsel<=0)
                {		  
                		 if(ifself!=null)
                		{
                		
                			
                		                               if($('div[data-id=' + dataId + ']')!=null)
                			                          {
                			                          	
                			                          
                			                          	   
                			                             $('div[data-id=' + dataId + ']').animate({'height': 0}, function()
                                                        {
                                                              $(this).remove();
                                                              calcTotalDayAgain();
                                                              calcScroll();
                                                        });  
                
                                                      
                	                                   	$('div[data-date2=' + dataId + ']').find("i").remove();
                			                          	
                			                          } 
                			
          //--------------------------------------------------
                			   
                			   
                		}
                		else
                		{                                   
                				$('div[data-date2=' + dataId + ']').removeClass("have-event").removeClass("selected");
                			   	$('div[data-date2=' + dataId + ']').find("i").remove();
                			
                		}
                		
                }		
                		*/
                
               { 
          
             /*  	if(ifself!=null)
               	{
               		
              alert("已添加过了");
               	}
               	else  *
               	{
                $this.prepend(div('div', 'added-event').attr({'data-date':thisDay, 'data-time': time, 'data-title': title,'id':"event-"+dataId, 'data-id': dataId}));
                
                $this.find('.day.this-month.selected').prepend(
                    div('div','event-single').attr('data-id', dataId).append(
                       div('p','').text(title),
                        div('div','details').append
                        (
                            div('div', 'clock').text(thisDay),
                            div('div', 'erase').attr('data-datex', thisDay)
                        )
                    )
                ); 
                */
               /*
                $this.find('.day').has('.event-single').addClass('have-event').prepend(div('i',''));
                $this.find('.day').has('.event-single');//.attr("style","background:red");  //tom add
                
                //.prepend(div('i',''));
                $this.find('.events-list').html($this.find('.day.this-month.selected .event-single').clone())
                $this.find('.events-list .event-single').eq(0).hide().slideDown();
                calcTotalDayAgain();
                calcScroll();
                // scrolltop after adding new event
                $this.find('.events-list').scrollTop(0);
                // form reset
                $this.find('.add-new > input[type="text"]').val(lAddEvent[settings.lang]).select();
                */ 
                }
              
            
            
                
                 dataId++;
               
              //   alert("开始处理提交");
           
             var slist=$(".selected");
         //    alert(slist.length);
             var datalist="";
            for(var a=0;a<slist.length;a++)
            {
            	if(datalist=="")
            	 datalist=slist[a].innerHTML;
                else
                 datalist=datalist+"#"+slist[a].innerHTML;
                
            //	alert(slist[a].innerHTML);
            }
   var mstr=$(".header h1").text();
            	// mstr.replaceAll(" ","");
            	 var xindex=mstr.indexOf("月");
            	 var the_month=mstr.substring(0,xindex);
            	 var the_year=mstr.substring(mstr.length-4,mstr.length);
            
//     alert(datalist);
  
                 var w=plus.nativeUI.showWaiting("　　 正在提交  请等待...　　 \n");   
                
           	 var didvalue=localStorage.getItem('did');
           
           
                $.ajax({ type: "POST",
           url:"http://www.etjourney.com/project/baoche/app/server/newcalendar.php", 
           data:{
           	"did":didvalue,
           	 "datelist":datalist,
           	 "month":the_month,
           	 "year":the_year
           	 
           },
           //"did="+didvalue+"&day="+encodeURI(thisDay)+"&ifsel="+ifsel,
           dataType:"jsonp",
           jsonp:'callback', 
           timeout:30000,
           success: function(msg, textStatus)
           {
           	   if(msg.success=="1")
           	   {
           	  	plus.nativeUI.toast('成功提交');
           	   	
           	   }
           	   else
           	   {
           	  	plus.nativeUI.toast(msg.errinfo);
           	   	
           	   }
           	  	
           	  	w.close();
           }
           ,complete: function(XMLHttpRequest, textStatus) 
	       { 
	       
	        //  alert("100"+textStatus);
	       }
	       , error:function (XMLHttpRequest, textStatus, errorThrown)
	       {
	       	
	       	 
	       		plus.nativeUI.toast(textStatus);
	         	w.close();
	       	// alert(XMLHttpRequest.status);
	       //  alert(textStatus);
	       }
      });  
      
      
      
           	
                
                
            });
            
            $close.on('click', function(){
                prevAddEvent();  //tom cancel 
               // prevAddEventclick();
            });
            
            // delete event
            $this.on('click', '.event-single .erase', function()
            {
            	
                $('div[data-id=' + $(this).parents(".event-single").attr("data-id") + ']').animate({'height': 0}, 
                function()
                {
                	
                
                //	$(this).attr("data-date").removeClass("have-event");
                    $(this).remove();
                    calcTotalDayAgain();
                    calcScroll();
                });
                
                	 var x= $('.day');
                	var checkvalue= $(this).attr("data-datex");
                	
                	
                	 if(x!=null)
                	 {
                	 //	alert("check");
                	 //	alert(x.length);
                	 	for(var a=0;a<x.size();a++)
                	 	{
                	 	
                	 		var xvalue=x[a].getAttribute("data-date");
                	 	//	alert(xvalue);
                	 		if(xvalue!=null)
                	 		{
                	 		  if(xvalue==checkvalue)
                	 	     	{ 
                	 			
                	 			x[a].className="day this-month";
                	 			x[a].setAttribute("style","");
                	 		
                	 			break;
                	 		  }
                	 		}
                	 	}
                	 	
                	 }
          
          
              
             // alert($(this).attr("data-datex"));
                	// $('div[data-data=' + $(this).parents(".event-single").attr("data-date") + ']')
                
            });

            
            
          
         refreshcalendar();
        

        };
 

       

    }(jQuery));

});

