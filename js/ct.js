﻿
var CT=CT||{};
CT.t=[];
CT.f=[];
CT.s=[];
CT.buildRunTime=function(endTime,startTime)
{var restTime=endTime-startTime;
	var t={};
	t.day=Math.floor(restTime/(24*60*60*1000));
	t.hour=Math.floor(restTime/(60*60*1000)%24);
	t.min=Math.floor(restTime/(60*1000)%60);
	t.second=Math.floor(restTime/1000%60);
	return t;
};
CT.justRunIt=function(endTime,startTime,callback)
{
	var t=this.buildRunTime(endTime,startTime);
	if(t.day<0)
	{callback(null);return;}
	if(t.day<10)
	{t.day="0"+t.day;}
	if(t.hour<10)
	{t.hour="0"+t.hour;}
	if(t.min<10)
	{t.min="0"+t.min;}
	if(t.second<10)
	{t.second="0"+t.second;}
	if(arguments.length==3)
	{callback(t);return;}
};
CT.push=function(t,s,f)
{
	if(!t||!s||!f)
	return;
	this.t.push(t);
	this.f.push(f);
	this.s.push(s);
	};
	CT.run=function()
	{this.init();for(var i=0,j=this.t.length;i<j;i++){this.s[i]=this.s[i]+1000;this.justRunIt(this.t[i],this.s[i],this.f[i]);}};
	CT.init=function()
	{setTimeout(function(){CT.run();},1000);};